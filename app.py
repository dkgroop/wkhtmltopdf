#! /usr/bin/env python
"""
    WSGI APP to convert wkhtmltopdf As a webservice

    :copyright: (c) 2013 by Openlabs Technologies & Consulting (P) Limited
    :license: BSD, see LICENSE for more details.
"""
import json
import tempfile

from werkzeug.wsgi import wrap_file
from werkzeug.wrappers import Request, Response
from executor import execute


@Request.application
def application(request):
    """
    To use this application, the user must send a POST request with
    base64 or form encoded encoded HTML content and the wkhtmltopdf Options in
    request data, with keys 'base64_html' and 'options'.
    The application will return a response with the PDF file.
    """
    if request.method != 'POST':
        return

    request_is_json = request.content_type.endswith('json')

    payload = json.loads(request.data)
    options = payload.get('options', {})
    inurl = payload.get('inurl', '')
    outname = payload.get('out', '')

    args = ['wkhtmltopdf']
    if options:
        for option, value in options.items():
            args.append('--%s' % option)
            if value:
                args.append('"%s"' % value)

    args += [inurl, outname + ".pdf"]

    # Execute the command using executor
    execute(' '.join(args))

    return Response(
        wrap_file(request.environ, open(outname + '.pdf')),
        mimetype='application/pdf',
    )

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple(
        '127.0.0.1', 5000, application, use_debugger=True, use_reloader=True
    )